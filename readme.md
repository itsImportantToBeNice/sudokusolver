## SudokuSolver

This is a little java test project for solving sudokus.

See SolverTest class for using examples. But basically just create a Sudoku with your fields (zeros for empty fields), give it into a solver object and execute the solve() function.
