package sudokusolver.models;

import de.itsimportanttobenice.sudokusolver.exceptions.InvalidBoxSizeException;
import de.itsimportanttobenice.sudokusolver.exceptions.InvalidFieldsException;
import de.itsimportanttobenice.sudokusolver.models.Sudoku;

public class SudokuTest extends Sudoku {
    private int[][] solution;

    public SudokuTest(int[][] fields, int boxSizeRow, int boxSizeCol, int[][] solution) throws InvalidBoxSizeException, InvalidFieldsException {
        super(fields, boxSizeRow, boxSizeCol);
        setSolution(solution);
    }

    private void setSolution(int[][] solution) {
        this.solution = solution;
    }

    public int[][] getSolution() {
        return this.solution;
    }
}
