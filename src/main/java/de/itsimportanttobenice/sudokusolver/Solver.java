package de.itsimportanttobenice.sudokusolver;

import de.itsimportanttobenice.sudokusolver.exceptions.SolvingNotPossibleException;
import de.itsimportanttobenice.sudokusolver.models.Sudoku;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Solver {
    Sudoku sudoku;
    private boolean guessing = false;
    private int guessDepth = 3;
    private int[][] temporaryFields;

    public Solver(Sudoku sudoku) {
        setSudoku(sudoku);
    }

    public Solver(Sudoku sudoku, boolean guessing) {
        setSudoku(sudoku);
        setGuessing(guessing);
    }

    public Solver(Sudoku sudoku, boolean guessing, int guessDepth) {
        setSudoku(sudoku);
        setGuessing(guessing);
        setGuessDepth(guessDepth);
    }

    public void solve() throws SolvingNotPossibleException {
        replaceNextSinglePossibility();
    }

    private void replaceNextSinglePossibility() throws SolvingNotPossibleException {
        for (int i = 0; i < getSudoku().getFields().length; i++) {
            for (int j = 0; j < getSudoku().getFields()[i].length; j++) {
                int currentValue = getSudoku().getField(i, j);
                if (currentValue != 0) {
                    continue;
                }
                List<Integer> possibleValues = getPossibilities(i, j);
                if (possibleValues.size() == 0) {
                    throw new SolvingNotPossibleException("possible values for row " + i + " and col " + j + " ist a empty list on depth " + getGuessDepth());
                } else if (possibleValues.size() == 1) {
                    System.out.println("Setting " + possibleValues.get(0) + " at position (" + i + "," + j + ")");
                    getSudoku().setField(i, j, possibleValues.get(0));
                    prettyPrintSudoku();
                    replaceNextSinglePossibility();
                    return;
                }
            }
        }

        if (!isReady()) {
            if (isGuessing()) {
                guess();
                return;
            }
            throw new SolvingNotPossibleException("all fields can be filled with more than one option on depth " + getGuessDepth());
        }
    }

    private void guess() throws SolvingNotPossibleException {
        System.out.println("Guessing on depth " + getGuessDepth());
        if (getGuessDepth() <= 0) {
            throw new SolvingNotPossibleException("guessing depth exceeded");
        }

        for (int i = 0; i < getSudoku().getFields().length; i++) {
            for (int j = 0; j < getSudoku().getFields()[i].length; j++) {
                int currentValue = getSudoku().getField(i, j);
                if (currentValue != 0) {
                    continue;
                }
                List<Integer> possibleValues = getPossibilities(i, j);
                if (possibleValues.size() == 2) {
                    int guessedValue = possibleValues.get(0);
                    copyFields();
                    System.out.println("Setting " + possibleValues.get(0) + " at position (" + i + "," + j + ") on depth " + getGuessDepth());
                    sudoku.setField(i, j, guessedValue);
                    prettyPrintSudoku();

                    Solver solver = new Solver(sudoku, isGuessing(), getGuessDepth() - 1);
                    try {
                        solver.solve();
                        return;
                    } catch (SolvingNotPossibleException e) {
                        guessedValue = possibleValues.get(1);
                        System.out.println("Resetting fields on depth " + getGuessDepth());
                        resetFields();
                        System.out.println("Setting " + possibleValues.get(1) + " at position (" + i + "," + j + ") on depth " + getGuessDepth());
                        sudoku.setField(i, j, guessedValue);
                        prettyPrintSudoku();

                        solver = new Solver(sudoku, isGuessing(), getGuessDepth() - 1);
                        try {
                            solver.solve();
                            return;
                        } catch (SolvingNotPossibleException e2) {
                            System.out.println("Resetting fields on depth " + getGuessDepth());
                            resetFields();
                        }
                    }
                }
            }
        }

        throw new SolvingNotPossibleException("guessing cannot find a solution for a field with only two possible values left");
    }

    private List<Integer> getPossibilities(int i, int j) {
        List<Integer> possibleValues = getPossibleValues();
        List<Integer> rowOccurrences = getRowOccurrences(i);
        List<Integer> colOccurrences = getColOccurrences(j);
        List<Integer> boxOccurrences = getBoxOccurrences(i, j);
        possibleValues.removeAll(rowOccurrences);
        possibleValues.removeAll(colOccurrences);
        possibleValues.removeAll(boxOccurrences);

        return possibleValues;
    }

    private List<Integer> getPossibleValues() {
        List<Integer> possibleValues = new ArrayList<>();
        for (int i = 0; i < getSudoku().getFields().length; i++) {
            possibleValues.add(i + 1);
        }

        return possibleValues;
    }

    private boolean isReady() {
        for (int i = 0; i < getSudoku().getFields().length; i++) {
            for (int j = 0; j < getSudoku().getFields()[i].length; j++) {
                if (getSudoku().getField(i, j) == 0) {
                    return false;
                }
            }
        }

        return true;
    }

    private List<Integer> getRowOccurrences(int row) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < getSudoku().getFields().length; i++) {
            if (getSudoku().getField(row, i) != 0) {
                numbers.add(getSudoku().getField(row, i));
            }
        }

        return numbers;
    }

    private List<Integer> getColOccurrences(int col) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < getSudoku().getFields().length; i++) {
            if (getSudoku().getField(i, col) != 0) {
                numbers.add(getSudoku().getField(i, col));
            }
        }

        return numbers;
    }

    private List<Integer> getBoxOccurrences(int row, int col) {
        List<Integer> numbers = new ArrayList<>();
        int minX = (row / getSudoku().getBoxSizeCol()) * getSudoku().getBoxSizeCol();
        int minY = (col / getSudoku().getBoxSizeRow()) * getSudoku().getBoxSizeRow();
        int maxX = minX + getSudoku().getBoxSizeCol();
        int maxY = minY + getSudoku().getBoxSizeRow();

        for (int i = minX; i < maxX; i++) {
            for (int j = minY; j < maxY; j++) {
                if (getSudoku().getField(i, j) != 0) {
                    numbers.add(getSudoku().getField(i, j));
                }
            }
        }

        return numbers;
    }

    private void copyFields() {
        temporaryFields = new int[getSudoku().getFields().length][getSudoku().getFields().length];
        for (int i = 0; i < getSudoku().getFields().length; i++) {
            for (int j = 0; j < getSudoku().getFields()[i].length; j++) {
                temporaryFields[i][j] = getSudoku().getField(i, j);
            }
        }
    }

    private void resetFields() {
        for (int i = 0; i < temporaryFields.length; i++) {
            for (int j = 0; j < temporaryFields[i].length; j++) {
                getSudoku().setField(i, j, temporaryFields[i][j]);
            }
        }
    }

    private void prettyPrintSudoku() {
        System.out.println("---------- Guess Depth " + getGuessDepth() + " -----------");
        for (int i = 0; i < getSudoku().getFields().length; i++) {
            for (int j = 0; j < getSudoku().getFields()[i].length; j++) {
                System.out.print(String.format("%2d", getSudoku().getFields()[i][j]) + "\t");
            }
            System.out.println("");
        }
    }
}
