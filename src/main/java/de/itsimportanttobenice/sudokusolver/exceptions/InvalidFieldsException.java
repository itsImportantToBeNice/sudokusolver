package de.itsimportanttobenice.sudokusolver.exceptions;

public class InvalidFieldsException extends Exception {
    public InvalidFieldsException(String message) {
        super("Fields are not valid: " + message);
    }
}
