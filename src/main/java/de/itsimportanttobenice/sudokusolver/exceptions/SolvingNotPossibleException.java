package de.itsimportanttobenice.sudokusolver.exceptions;

public class SolvingNotPossibleException extends Exception {
    public SolvingNotPossibleException(String message) {
        super("Solving soduku is not possible: " + message);
    }
}
