package de.itsimportanttobenice.sudokusolver.exceptions;

public class InvalidBoxSizeException extends Exception {
    public InvalidBoxSizeException(String message) {
        super("Box size is invalid: " + message);
    }
}
