package de.itsimportanttobenice.sudokusolver.models;

import de.itsimportanttobenice.sudokusolver.exceptions.InvalidBoxSizeException;
import de.itsimportanttobenice.sudokusolver.exceptions.InvalidFieldsException;
import lombok.Data;

@Data
public class Sudoku {
    private int maxValue;
    private int[][] fields;
    private int boxSizeRow;
    private int boxSizeCol;

    public Sudoku(int[][] fields, int boxSizeRow, int boxSizeCol) throws InvalidBoxSizeException, InvalidFieldsException {
        setFields(fields);
        setBoxSizes(boxSizeRow, boxSizeCol);
        setMaxValue(calculateMaxValue());
    }

    private int calculateMaxValue() {
        return getFields().length + 1;
    }

    public void setFields(int[][] fields) throws InvalidFieldsException {
        int fieldsLength = fields.length;
        for (int i = 0; i < fieldsLength; i++) {
            if (fieldsLength != fields[i].length) {
                throw new InvalidFieldsException("Rows and cols must be the same size!");
            }
        }
        this.fields = fields;
    }

    private void setBoxSizes(int boxSizeRow, int boxSizeCol) throws InvalidBoxSizeException {
        if (getFields().length % boxSizeCol != 0) {
            throw new InvalidBoxSizeException("containing fields size of col boxes must be the same");
        }
        for (int i = 0; i < getFields().length; i++) {
            if (getFields()[i].length % boxSizeRow != 0) {
                throw new InvalidBoxSizeException("containing fields size of row boxes must be the same");
            }
        }
        setBoxSizeRow(boxSizeRow);
        setBoxSizeCol(boxSizeCol);
    }

    private void setBoxSizeRow(int boxSizeRow) {
        this.boxSizeRow = boxSizeRow;
    }

    private void setBoxSizeCol(int boxSizeCol) {
        this.boxSizeCol = boxSizeCol;
    }

    public int getField(int i, int j) {
        return getFields()[i][j];
    }

    public void setField(int i, int j, int value) {
        getFields()[i][j] = value;
    }
}
